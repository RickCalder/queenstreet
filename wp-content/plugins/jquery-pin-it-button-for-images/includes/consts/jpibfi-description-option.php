<?php

abstract class JPIBFIDescriptionOption
{
    const PageTitle = 1;
    const PageDescription = 2;
    const ImageTitleOrAlt = 3;
    const SiteTitle = 4;
    const ImageDescription = 5;
    const ImageAlt = 6;
}