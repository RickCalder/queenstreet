{
    "debug": false,
    "featureManagerIsAlwaysOnlineEnabled": true,
    "featureManagerIsBrowserCacheTTLEnabled": true,
    "featureManagerIsBrowserIntegrityCheckEnabled": true,
    "featureManagerIsCacheLevelEnabled": true,
    "featureManagerIsChallengePassageEnabled": true,
    "featureManagerIsDevelopmentModeEnabled": true,
    "featureManagerIsFullZoneProvisioningEnabled": false,
    "featureManagerIsImageOptimizationEnabled": true,
    "featureManagerIsIpv6Enabled": true,
    "featureManagerIsIpRewriteEnabled": true,
    "featureManagerIsMinifyEnabled": true,
    "featureManagerIsProtocolRewriteEnabled": true,
    "featureManagerIsPurgeCacheEnabled": true,
    "featureManagerIsRailgunEnabled": true,
    "featureManagerIsScanEnabled": false,
    "featureManagerIsSecurityLevelEnabled": true,
    "featureManagerIsSSLEnabled": true,
    "featureManagerIsWAFEnabled": true,
    "isSubdomainCheckEnabled": true,
    "homePageCards": ["ApplyDefaultSettingsCard", "PurgeCacheCard", "PluginSpecificCacheCard"],
    "moreSettingsCards": {
        "container.moresettings.speed": ["AlwaysOnlineCard", "ImageOptimizationCard"],
        "container.moresettings.security": ["SecurityLevelCard", "WAFCard", "AdvanceDDoSCard"]
    },
    "locale": "en",
    "integrationName": "wordpress",
    "useHostAPILogin": false,
    "version": "3.0.2"
}