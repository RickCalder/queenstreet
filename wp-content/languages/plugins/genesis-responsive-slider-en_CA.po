# Translation of Stable (latest release) in English (Canada)
# This file is distributed under the same license as the Stable (latest release) package.
msgid ""
msgstr ""
"PO-Revision-Date: 2015-10-03 17:30:47+0000\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: GlotPress/1.0-alpha-1100\n"
"Project-Id-Version: Stable (latest release)\n"

#: admin.php:33
msgid "[Continue Reading]"
msgstr "[Continue Reading]"

#: admin.php:292
msgid "Title"
msgstr "Title"

#: admin.php:77
msgid "Settings reset."
msgstr "Settings reset."

#: admin.php:79
msgid "Settings saved."
msgstr "Settings saved."

#: admin.php:92
msgid "Slider Settings"
msgstr "Slider Settings"

#: admin.php:114
msgid "Genesis Responsive Slider Settings"
msgstr "Genesis Responsive Slider Settings"

#: admin.php:152 genesis-responsive-slider.php:210
msgid "Genesis - Responsive Slider"
msgstr "Genesis - Responsive Slider"

#: admin.php:153 admin.php:164
msgid "Save Settings"
msgstr "Save Settings"

#: admin.php:154 admin.php:165
msgid "Reset Settings"
msgstr "Reset Settings"

#: admin.php:154
msgid "Are you sure you want to reset?"
msgstr "Are you sure you want to reset?"

#: admin.php:194
msgid "Type of Content"
msgstr "Type of Content"

#: admin.php:196
msgid "Would you like to use posts or pages"
msgstr "Would you like to use posts or pages"

#: admin.php:215
msgid "By Taxonomy and Terms"
msgstr "By Taxonomy and Terms"

#: admin.php:215
msgid "Choose a term to determine what slides to include"
msgstr "Choose a term to determine what slides to include"

#: admin.php:219
msgid "All Taxonomies and Terms"
msgstr "All Taxonomies and Terms"

#: admin.php:246
msgid "Include or Exclude by Taxonomy ID"
msgstr "Include or Exclude by Taxonomy ID"

#: admin.php:249
msgid "List which category, tag or other taxonomy IDs to exclude. (1,2,3,4 for example)"
msgstr "List which category, tag or other taxonomy IDs to exclude. (1,2,3,4 for example)"

#: admin.php:259
msgid "Include or Exclude by %s ID"
msgstr "Include or Exclude by %s ID"

#: admin.php:262
msgid "Choose the include / exclude slides using their post / page ID in a comma-separated list. (1,2,3,4 for example)"
msgstr "Choose the include / exclude slides using their post / page ID in a comma-separated list. (1,2,3,4 for example)"

#: admin.php:266
msgid "Select"
msgstr "Select"

#: admin.php:267
msgid "Include"
msgstr "Include"

#: admin.php:268
msgid "Exclude"
msgstr "Exclude"

#: admin.php:273
msgid "List which"
msgstr "List which"

#: admin.php:273 admin.php:293
msgid "ID"
msgstr "ID"

#: admin.php:273
msgid "to include / exclude. (1,2,3,4 for example)"
msgstr "to include / exclude. (1,2,3,4 for example)"

#: admin.php:279
msgid "Number of Slides to Show"
msgstr "Number of Slides to Show"

#: admin.php:284
msgid "Number of Posts to Offset"
msgstr "Number of Posts to Offset"

#: admin.php:289
msgid "Order By"
msgstr "Order By"

#: admin.php:291
msgid "Date"
msgstr "Date"

#: admin.php:294
msgid "Random"
msgstr "Random"

#: admin.php:302
msgid "Transition Settings"
msgstr "Transition Settings"

#: admin.php:305
msgid "Time Between Slides (in milliseconds)"
msgstr "Time Between Slides (in milliseconds)"

#: admin.php:310
msgid "Slide Transition Speed (in milliseconds)"
msgstr "Slide Transition Speed (in milliseconds)"

#: admin.php:315
msgid "Slider Effect"
msgstr "Slider Effect"

#: admin.php:316 admin.php:365
msgid "Select one of the following:"
msgstr "Select one of the following:"

#: admin.php:318
msgid "Slide"
msgstr "Slide"

#: admin.php:319
msgid "Fade"
msgstr "Fade"

#: admin.php:325
msgid "Display Settings"
msgstr "Display Settings"

#: admin.php:328
msgid "Maximum Slider Width (in pixels)"
msgstr "Maximum Slider Width (in pixels)"

#: admin.php:333
msgid "Maximum Slider Height (in pixels)"
msgstr "Maximum Slider Height (in pixels)"

#: admin.php:338
msgid "Display Next / Previous Arrows in Slider?"
msgstr "Display Next / Previous Arrows in Slider?"

#: admin.php:342
msgid "Display Pagination in Slider?"
msgstr "Display Pagination in Slider?"

#: admin.php:347
msgid "Content Settings"
msgstr "Content Settings"

#: admin.php:350
msgid "Do not link Slider image to Post/Page."
msgstr "Do not link Slider image to Post/Page."

#: admin.php:354
msgid "Display Post/Page Title in Slider?"
msgstr "Display Post/Page Title in Slider?"

#: admin.php:357
msgid "Display Content in Slider?"
msgstr "Display Content in Slider?"

#: admin.php:361
msgid "Hide Title & Content on Mobile Devices"
msgstr "Hide Title & Content on Mobile Devices"

#: admin.php:367
msgid "Display post content"
msgstr "Display post content"

#: admin.php:368
msgid "Display post excerpts"
msgstr "Display post excerpts"

#: admin.php:373
msgid "More Text (if applicable)"
msgstr "More Text (if applicable)"

#: admin.php:378
msgid "Limit content to"
msgstr "Limit content to"

#: admin.php:380
msgid "characters"
msgstr "characters"

#: admin.php:383
msgid "Using this option will limit the text and strip all formatting from the text displayed. To use this option, choose \"Display post content\" in the select box above."
msgstr "Using this option will limit the text and strip all formatting from the text displayed. To use this option, choose \"Display post content\" in the select box above."

#: admin.php:386
msgid "Slider Excerpt Width (in percentage)"
msgstr "Slider Excerpt Width (in percentage)"

#: admin.php:391
msgid "Excerpt Location (vertical)"
msgstr "Excerpt Location (vertical)"

#: admin.php:393
msgid "Top"
msgstr "Top"

#: admin.php:394
msgid "Bottom"
msgstr "Bottom"

#: admin.php:399
msgid "Excerpt Location (horizontal)"
msgstr "Excerpt Location (horizontal)"

#: admin.php:401
msgid "Left"
msgstr "Left"

#: admin.php:402
msgid "Right"
msgstr "Right"

#: admin.php:412
msgid "Save Changes"
msgstr "Save Changes"

#: genesis-responsive-slider.php:208
msgid "Displays a slideshow inside a widget area"
msgstr "Displays a slideshow inside a widget area"

#: genesis-responsive-slider.php:371
msgid "Title:"
msgstr "Title:"

#: genesis-responsive-slider.php:374
msgid "To configure slider options, please go to the <a href=\"%s\">Slider Settings</a> page."
msgstr "To configure slider options, please go to the <a href=\"%s\">Slider Settings</a> page."

#. Plugin Name of the plugin/theme
msgid "Genesis Responsive Slider"
msgstr "Genesis Responsive Slider"

#. #-#-#-#-#  tmp-genesis-responsive-slider.pot (Genesis Responsive Slider
#. 0.9.4)  #-#-#-#-#
#. Plugin URI of the plugin/theme
#. #-#-#-#-#  tmp-genesis-responsive-slider.pot (Genesis Responsive Slider
#. 0.9.4)  #-#-#-#-#
#. Author URI of the plugin/theme
msgid "http://www.studiopress.com"
msgstr "http://www.studiopress.com"

#. Description of the plugin/theme
msgid "A responsive featured slider for the Genesis Framework."
msgstr "A responsive featured slider for the Genesis Framework."

#. Author of the plugin/theme
msgid "StudioPress"
msgstr "StudioPress"
